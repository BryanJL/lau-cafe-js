var now = new Date();
var people = [];
var counter = 0;

var PeopleStates = {
    IDLE: 0,
    WALKING: 1,
    ACTING: 2
}

class Person {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.path = [];
        this.actionTimer = -1;
        this.name = counter++;
        this.state = PeopleStates.IDLE;
    }
    
    // Handle the general things, like walking and acting
    update(interval) {
        switch(this.state) {
            case PeopleStates.WALKING:
                walk();
                break;
            default:
                break;
        }
        
        // For more specialized things, return the remainder
        return interval;
    }
}

class Patron extends Person {
    constructor() {
        super();
        this.state = PeopleStates.WALKING;
    }
    
    update(interval) {
        var remainingTime = super.update(interval);
    }
}

function reset() {
    alert('reset!');
    counter = 0;
    people.length = 0;
    update(0);
}

function update(interval) {
    var divString = people.length + "<br>";
    
    for(var person of people) {
        person.x += interval / 1000.0;
        divString += "Person " + person.name + " at " +
            person.x + ", " + person.y + "<br>";
    }
    $('div').html(divString);
}

function spawnPerson() {
    people.push(new Patron);
    update(0);
}